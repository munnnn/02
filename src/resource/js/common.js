var ui = {
    headerControl: function() {
        if($(document).scrollTop() > $('.header').outerHeight() ){
            $('.header').addClass('active');
        }else{
            $('.header').removeClass('active');
		}
	},
	navControl : function(){
		var trigger = $('.nav li button');
		var trigger2 = $('.m-trigger');
		trigger.on('click', function(){
			$(this).closest('li').siblings().find('button').removeClass('active');
            if($(this).is('.active')){
				$(this).removeClass('active')
			}else{
				$(this).addClass('active')
			}
		});
		trigger2.on('click', function(){
            if($(this).is('.active')){    
				$(this).removeClass('active').next('.nav').removeClass('active')
			}else{
				$(this).addClass('active').next('.nav').addClass('active')
			}
		});

		panel = $('section');
    
		for(var i=0; i<$(trigger).length; i++){
			trigger.eq(i).attr('data-trigger-index' ,i);
			panel.eq(i).attr('data-panel-index', i)
		}
		trigger.on('click', function() {
			var index = $(this).data('trigger-index');
			var _top = $('[data-panel-index=' + index + ']').offset().top;
			
			$('html, body').animate({scrollTop:_top}, '300');
		})
	},
	writeText : function(){
		var typingBool = false; 
		var typingIndex=0; 
		var liIndex = 0;
		var liLength = $(".typing-txt>ul>li").length;
		var typingTxt = $(".typing-txt>ul>li").eq(liIndex).text(); 

		typingTxt=typingTxt.split("");
		if(typingBool==false){
			typingBool=true; 
			var tyInt = setInterval(typing,100);
		} 
     
		function typing(){ 
		if(typingIndex<typingTxt.length){
			$(".typing").append(typingTxt[typingIndex]); 
			typingIndex++; 
		} else{
			
			if(liIndex>=liLength-1){
				liIndex=0;
			}else{
				liIndex++; 
			}
			
			
			typingIndex=0;
			typingBool = false; 
			typingTxt = $(".typing-txt>ul>li").eq(liIndex).text(); 
			
			clearInterval(tyInt);
				setTimeout(function(){
					$(".typing").html('');
					tyInt = setInterval(typing,100);
				},1000);
			} 
		}  
	}
};
/* event */
$(document).bind({
	'ready' : function(){
        ui.navControl();
        ui.writeText();
	},
	'load' : function(){
	},
	'scroll' : function(){
        ui.headerControl();
	},
	'resize' : function(){
	}
})