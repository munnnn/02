const gulp = require('gulp'),
  sass = require('gulp-sass'),
  sourcemap = require('gulp-sourcemaps'),
  browser = require('browser-sync'),
  connectSSI = require('connect-ssi'),
  reload = browser.reload,
  del = require('del'),
  ssi = require("gulp-ssi"),
  autoprefix = require('gulp-autoprefixer'),
  imagemin = require('gulp-imagemin')

const clean = () => del(['./dist']);

const src = './src'
const dist = './dist'
const path = {
  main: src,
  scss: `${src}/resource/scss/**/*.scss`,
  css: `${src}/resource/css/`,
  js: `${src}/resource/js/**/*.js`,
  library: `${src}/resource/library/**/*.js`,
  images: `${src}/resource/images/**/*.+(jpg|jpeg|png|gif)`,
  font: `${src}/resource/images/**/*.+(otf|woff|woff2)`,
  html: `${src}/**/*.html`
}
const scssOptions = {
  outputStyle : "compact"
}

gulp.task('sass', () => {
  return gulp
    .src(path.scss)
    .pipe(sourcemap.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemap.write())
    .pipe(gulp.dest(path.css))
    .pipe(browser.stream())
})

gulp.task('serv', () => {
  browser.init({
    // open: 'external',
    server: {
      baseDir: path.main,
      middleware: [connectSSI({
          baseDir: __dirname + '/src/',
          ext: '.html'
      })],
    }
  })

  gulp.watch(path.scss, gulp.series('sass'))
  gulp.watch(path.js).on('change', reload)
  gulp.watch(path.images).on('change', reload)
  gulp.watch(path.html).on('change', reload)
})

const html = () => {
  return gulp
    .src(path.html)
    .pipe(ssi({
      root: path.main
    }))
    .pipe(gulp.dest(dist))
}

const image = () => {
  return gulp
      .src([path.images])
      .pipe(imagemin())
      .pipe(gulp.dest(`${dist}/resource/images`))
}

const fonts = () => {
    return gulp
        .src(path.font)
        .pipe(gulp.dest(`${dist}/resource/images`))
}

const css = () => {
    return gulp
      .src(path.scss)
      .pipe(sass(scssOptions).on('error', sass.logError))
      .pipe(autoprefix({browsers: ['> 1%', 'last 2 versions', 'firefox >= 4', 'safari 7', 'safari 8', 'IE 8', 'IE 9', 'IE 10', 'IE 11'], cascade: false}))
      .pipe(gulp.dest(`${dist}/resource/css`))
}

const js = () => {
  return (
      gulp
        .src(path.js)
        .pipe(gulp.dest(`${dist}/resource/js`))
  )
}

const lib = () => {
  return (
      gulp
        .src(path.library)
        .pipe(gulp.dest(`${dist}/resource/library`))
  )
}

const build = gulp.series(
  clean,
  html,
  fonts,
  css,
  image,
  js,
  lib
)

gulp.task('default', gulp.series('sass', 'serv'))
gulp.task('build', build)